class PhotosController < ApplicationController
  before_filter :user_can?
  before_action :set_photo, only: [:edit, :update, :destroy]
  before_action :set_shop, only:[:index, :create]
  def index
    @photos = HeaderPhoto.where("se_shop_shop_id = ?", @shop.id)
  end

  def new
    @photo = HeaderPhoto.new
  end

  def edit
  end

  def create
    @photo = HeaderPhoto.new(photo_params)
    @photo.se_shop_shop_id = @shop.id
    respond_to do |format|
      if @photo.save
        format.html{redirect_to shop_photos_path, notice: 'Новое изображения для главной страницы добавлено'}
      else
        format.html{render :new}
      end
    end
  end

  def update
    @old = HeaderPhoto.find_by_position(photo_params[:position])
    if @old
      @old.update_attribute(:position, nil)
    end
    respond_to do |format|
      if @photo.update_attribute(:position, photo_params[:position])
        format.html{redirect_to photos_path, notice: 'Новое изображения для главной страницы добавлено'}
        format.js{}
        format.json{render :json => {:status => :ok}}
      else
        format.html{render :edit}
        format.js{}
        format.json{render :json => {:status => :bad_request}}
      end
    end
  end

  def destroy
    @photo.destroy
    respond_to do |format|
      format.html{redirect_to shop_photos_path}
      format.js{render :layout => false}
      format.json{render :json => {:status => :ok}, notice: "Изображение удалено"}
    end
  end


  private

  def set_photo
    @photo = HeaderPhoto.find(params[:id])
  end

  def photo_params
    params.require(:header_photo).permit(:image, :position, :shop_id)
  end

  def set_shop
    @shop = SeShop::Shop.friendly.find(params[:shop_id])
  end

  def user_can?
    if user_admin?

    else
      redirect_to root_path
      flash[:alert] = "Доступ запрещен"
    end
  end

  def change_position

  end

end
