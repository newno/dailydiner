class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :prepare_meta_tags, if: "request.get?"
  before_action :set_main_shop, if: "request.get?"

  helper_method :user_admin?, :get_code, :get_phone, :set_photos_block

  def prepare_meta_tags(options={})

    title       = "Daily Diner"
    site        = "Daily Diner"
    site_name   = "Daily Diner"
    description = "Daily Diner - вкусная еда"
    image       = options[:image] || view_context.image_path("bg-image.jpg")
    current_url = request.url

    # Let's prepare a nice set of defaults

    defaults = {
        site:        site_name,
        image:       image,
        description: description,
        keywords:    %w[Барнаул пицца андеграунд кафе недорого поесть],
    }


    options.reverse_merge!(defaults)


    set_meta_tags options

  end

  def title(page_title)
    content_for :title, page_title
  end

  def after_sign_in_path_for(resource)
    if current_user.role_id == 1
      se_shop.admin_root_path
    else
      root_path
    end
  end

  def user_admin?
    if current_user && current_user.role_id == SeShop::Role.find_by_name('admin').id
      true
    else
      false
    end
  end

  def set_photos_block(shop)
    @photos_block = HeaderPhoto.where("se_shop_shop_id = ?", shop.id)
  end

  def get_code(phone)
      phone.to(phone.rindex(')'))
  end

  def get_phone(phone)
    phone.from(phone.rindex(')') + 1)
  end

  def set_main_shop
    @main_shop = SeShop::Shop.friendly.find('daily-diner')
  end

end

