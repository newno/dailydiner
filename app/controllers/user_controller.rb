class UserController < ApplicationController
  def logout
    render 'devise/sessions/logout'
  end
end
