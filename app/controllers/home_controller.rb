class HomeController < ApplicationController
  before_action :set_uuid, only: [:index]
  before_action :set_orders, only: [:index]
  before_action :set_main_shop, only: [:index, :doner, :cinnamon]
  def index
    @main_shop = SeShop::Shop.friendly.find('daily-diner')
    if @main_shop.se_shop_categories.where('tomain = ?', true)
      @categories =@main_shop.se_shop_categories.where('tomain = ?', true).first
      if @categories.present? && @categories.se_shop_items.present?
        @items = @categories.se_shop_items.limit(12)
      end

    end
    if SeShop::Shop.any?
      @shops = SeShop::Shop.all
    end

  end

  def doner
    @shop = SeShop::Shop.friendly.find('doner-street')
  end

  def cinnamon
    @shop = SeShop::Shop.friendly.find('kofe-bar-koritsa')
  end


  private
  def set_orders
    @orders = SeShop::Order.where('user_uuid = ? AND order_status_id = ?', @uuid, 1)
  end
  def set_uuid
    if cookies[:uuid].blank?
      cookies[:uuid] = SecureRandom.uuid
    end
    @uuid = cookies[:uuid]
  end

end
