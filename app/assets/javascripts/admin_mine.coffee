@orderPoller =
  poll: ->
    setTimeout @request, 10000

  request: ->
    if $('#orderReady .order-ready').length > 0
      $.get($('#orderReady').data('url'), after: $('#orderReady .order-ready').last().get(0).id)
    else
      $.get($('#orderReady').data('url'), after: $('#orderReady').data('last'))

  addOrder: (elem)->
    if $(elem).length > 0
      $('#orders #order-body').append($(elem).addClass('new-order'))
      @checkSound()
    @poll()

  checkSound: (stop) ->
    stop = stop || false
    l = $('#orderReady .status3, #orderReady .status4').length <= 0
    if stop
      if l
        @stopSound()
    else
      if !l
        @sound()
    false

  sound: ()->
    s = $('#notification-sound').get(0)
    s.play()
    t = 3000
    @timerId = setInterval (->
      s.play()
      return
    ), t
  stopSound: () ->
    clearInterval(@timerId)
    return


  setStatus: (elem, status, order_id) ->
    return $.ajax
      url: '/menu/admin/order_status'
      type: 'POST'
      data: {'status_id': status, "order_id": order_id}
      dataType: 'json'
      success: (data) ->
        orderPoller.refreshStatus(elem, data.newStatusId, data.newStatusName)
        orderPoller.checkSound(true)

  destroyOrder: (elem, id) ->
    parent = $(elem).parents('tbody.order-master')
    return $.ajax
      url: '/menu/admin/order_destroy'
      type: 'POST'
      data: {"order_id": id}
      dataType: 'json'
      success: (data) ->
#        orderPoller.checkSound(true)
        parent.fadeOut 500, ->
          setTimeout (->
            parent.animate {
              'opacity': 0
              'left': '-=500'
            }, 300, ->
              $(this).remove()
              orderPoller.checkSound(true)
          ), 300
          false

  refreshStatus: (elem, id, name) ->
    oldname = $(elem).data('name')
    parent = $(elem).parents('tbody.order-master')
    clearClass = parent.attr('class').replace(/\bstatus.*\b/g,"")
    parent.attr('class', clearClass + 'status'+(id-1))
    $(elem).parents('.manage-column').find('.statusNow').text oldname
    if id <= $('#order-body').data('max-status')
      if id <= 6
        parent.find('a.destroy').animate {
          'opacity': 0
        }, 300, ->
          $(this).remove()
      $(elem).data({'status': id, 'name':name}).text(name)
    else
      $(elem).fadeOut 500, ->
        $(this).remove()
        setTimeout (->
          parent.animate {
            'opacity': 0
            'left': '-=500'
          }, 300, ->
              $(this).remove()
        ), 5000
        false

    false


$(document).on "ready", ->
  if $('#orders').length > 0
    orderPoller.poll()
  return

$(document).on "click", "a.setStatus", ->
  if $(this).data('status') <= $('#order-body').data('max-status')
    orderPoller.setStatus($(this), $(this).data('status'), $(this).data('order-id'))
  false

$(document).on "click", "a.destroy", ->
  orderPoller.destroyOrder($(this), $(this).data('order-id'))
  false