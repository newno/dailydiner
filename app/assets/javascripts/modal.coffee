modalWindow = ".modal-window"
overlay = '.modal-overlay'
@modal =
  modalToggle: (elem, s1, s2) ->
    s1 = s1 || 500
    s2 = s2 || s1
    $modal = if elem[0]? then $(elem).parents(modalWindow) else $(modalWindow).not(":hidden")
    $(overlay).animate({
      'opacity': 'toggle'
    }, s1)
    $modal.animate({
      'opacity': 'toggle'
    }, s2)

$(document).on 'ready', ->


  $(overlay).on "click", ->
    modal.modalToggle(800, 400)
    return

  $('.modal-window .close-button').on "click", ->
    modal.modalToggle(800, 400)
    return