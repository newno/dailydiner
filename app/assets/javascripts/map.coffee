$(document).on "ready", ->
  if $('.contacts').length > 0
    setMap = (latlng) ->
      # Create a map object and specify the DOM element for display.
      map = new (google.maps.Map)(document.getElementById('map'),
        center: latlng
        scrollwheel: false
        zoom: 17)
      # Create a marker and set its position.
      marker = new (google.maps.Marker)(
        map: map
        position: latlng
        title: 'Hello World!')
      return

    geocodeAddress = (geocoder, address) ->
      geocoder.geocode {'address': address}, (results, status) ->
        setMap(results[0].geometry.location)
        return



    geocoder = new google.maps.Geocoder()
    if $('#map').length > 0
      $('.main').css('margin-bottom', 0)
      geocodeAddress(geocoder, $('.emblemBlock a.active').data('address'))

    footer = $('.contactContainer')

    $(document).on 'click', '.emblemBlock a', ->
      address = $(this).data('address')
      if address[0]? && address.length > 15
        geocodeAddress(geocoder, address)

        footerAttr = {
          phone: $(this).data("phone"),
          street: $(this).data("street"),
          email: $(this).data("email")
          vklink: $(this).data("vklink")
          instalink: $(this).data("instalink")
        }
        footer.find('.phoneSpace span').text(footerAttr.phone.slice(footerAttr.phone.indexOf(")") + 1))
        footer.find('.phoneSpace span.small-text').text(footerAttr.phone.slice(0, footerAttr.phone.indexOf(")") + 1))
        footer.find('.address').text(footerAttr.street)
        footer.find('.email').text(footerAttr.email)
        footer.find('.vklink a').attr('href', 'http://'+footerAttr.vklink).text(footerAttr.vklink)
        footer.find('.instalink a').attr('href', 'http://'+footerAttr.instalink).text(footerAttr.instalink)

        $('.emblemBlock a.active').removeClass('active')
        $(this).addClass('active')
        cl = 'exist'
        info = '.map-box .shop-info'
        $(info + '.' + cl).removeClass(cl)
        $(info).eq($(this).index()).addClass(cl)





      false



  return