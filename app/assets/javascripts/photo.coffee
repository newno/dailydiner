$.fn.extend({
  outerHTML: (s) ->
    if s then @before(s).remove() else jQuery('<p>').append(@eq(0).clone()).html()
  hideAnimate: (aSpeed) ->
    aSpeed = aSpeed || 300
    this.animate({'opacity':'0'}, aSpeed, ->
      this.remove()
    )
    return this
  hideBox: (aSpeed, c) ->
    aSpeed = aSpeed || 300
    this.animate({'opacity':'0'}, aSpeed, ->
      this.css('display','none')
      c()
    )
    return this
  animateToggle: (aSpeed) ->
    aSpeed = aSpeed || 300
    this.animate({'opacity':'toogle'}, {duration: aSpeed})
    return this
})

photoSlider =
  w: ->
    $('.photo-head').data('boxW')

  startSlide: ->
    setTimeout @slide, 5000

  slide: ->

    head = $('.photo-head')
    active = "showPhoto"
    activeBox = head.find('.photo-box.showPhoto')
    last = activeBox.eq(activeBox.length-1)
    next = last.next()
    left = -photoSlider.w()


    if head.find('.photo-box').length > 3
      puls= ->
        activeBox.eq(0).removeClass(active)
        next.addClass(active)
      head.animate {'left': left}, 500, ->
        if head.find('.photo-box').length > 4
          puls()
        $(activeBox.eq(0)).appendTo(head)
        head.css('left', 0)



    photoSlider.startSlide()

  init: ->
    $('body').addClass('slider-init')
    @startSlide()


$(document).on "ready", ->

  if !$('body').hasClass('slider-init')
    photoSlider.init()
  $(document).on 'ajax:success', '.delete-link', (e, data, status, xhr) ->
    $(this).parents('.photo').hideAnimate()

  if $('.headerPhotos').length > 0
    if $('.photo-head').find('.photo-box').length > 3
      w = $('.photo-head').width()
      boxW = w*0.33 + 10
      $('.photo-head').data('boxW', boxW).width(w+boxW)
    $('.headerPhotos.fixHeight').height($('.photo-head .photo-box').width())



  return