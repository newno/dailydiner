(($) ->
  $.fn.setSize = () ->
    $this = $(this)
    $this.addClass('loading')
    size  = $this.data 'size' || false
    image = $this.data 'setimage' || false
    mobile =
      if  $(window).width() > 700
        false
      else
        $this.data 'mobile' || false

    if !$(this).data 'setSize'
      $width = $(window).width();
      $width = $width / size if size
      getType = ->
        $type = ''
        if $width < 400
          $type = 'w400_'
        else if $width < 700
          $type = 'w700_'
        else if $width < 1000
          $type = 'w1000_'
        else if $width < 1500
          $type = 'w1500_'
        else if $width < 2000
          $type = 'w2000_'
        else
          $type = ''
        return $type
      bg = $this.attr 'data-bg'
      if bg?
        type = getType.call($width)
        pos = bg.lastIndexOf('/')+1
        bg = bg.slice(0, pos) + type + bg.slice(pos)
        if !mobile
          if image
            $this.removeAttr('data-bg').data('setSize', true).attr('src', bg).load ->
              w = $(this).width()
              h = $(this).height()
              if w <= h
                $this.addClass('portain')
              if w * 3 <= h
                $this.addClass('soLong')
              if w == h || (w > h*0.9 && w < h*1.1)
                $this.addClass('squard')
              $this.removeClass('loading')
          else
            loadimg = $(new Image()).attr({'src': bg, 'style':"position:absolute; opacity: 0;"})
            bg = "url(" + bg + ")"
            loadimg.load ->
              $(this).remove()
              $this.removeClass('loading').removeAttr('data-bg').data('setSize', true).css 'background-image', bg
    return $this
) jQuery