# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "ready", ->

  if $('.menuContainer').length > 0
    cart = $('.cart-link')
    cart.data('top', cart.css('top'))
    cartOffset = $('.menuContainer').offset().top
    $('.menuContainer').data('offset', cartOffset)
    $(document).on 'scroll', onScroll
    $('body').data('wh', $(window).height()/2)

  parent = $('.emblemBlock .lenta')
  if parent.length > 0
    left =  parent.find('.active').get(0).offsetLeft + parent.find('.active').width()/2
    parent.css('margin-left', -left)

  $('.emblemBlock .lenta').on('click', 'a.active', ->
    return false
  )


onScroll = (event) ->
  scrollPos = $(document).scrollTop()
  $wH = $('body').data('wh')
  cartOffset = $('.menuContainer').data('offset')
  cart = $('.cart-link')
  cartTop = cart.data('top')
  if cartOffset - $wH >= scrollPos
    cart.addClass('stop')
  else
    cart.removeClass('stop')
  return
