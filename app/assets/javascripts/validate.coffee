$(document).on "ready", ->
  # Group of validation rules
  jQuery.validator.addMethod 'lettersonly', ((value, element) ->
    @optional(element) or /^[A-zА-яЁё ]+$/i.test(value)
  ), 'Введите только буквы'
  jQuery.validator.addMethod 'shipTime', ((value, element) ->
    @optional(element) or /(([0|1]\d|2[0-3]):[0-5]\d)|24:00/.test(value)
  ), 'Введите верный формат'
  $('#new_order_detail').validate
    errorPlacement: (error, element) ->
      if $(element).next(".error-box").length <= 0
        $(element).parent().append("<div class='error-box'></div>")
      $(element).next(".error-box").empty().append error.text()
    rules:
      'order_detail[name]':
        required: true
        rangelength: [2, 30]
        lettersonly: true
      'order_detail[street]':
        required: true
        rangelength: [2, 30]
      'order_detail[shipping_date]':
        shipTime: true
      'order_detail[phone]':
        required: true
      'order_detail[building]':
        required: true
        rangelength: [1, 5]
      'order_detail[email]':
        email: true
      'order_detail[apartment]':
        digits: true
      'order_detail[floor]':
        digits: true
      'order_detail[porch]':
        digits: true
  return
