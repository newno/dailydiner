$(document).on "ready", ->
  hideLabel = (field, noAnimate) ->
    noAnimate = noAnimate || false

    inp = $(field).children('input, textarea')
    label = $(field).children 'label'
    if inp.val()
      label.addClass('topLabelNOAnimate') if noAnimate
      setTimeout(->
        label.addClass 'topLabel'
        label.removeClass('topLabelNOAnimate') if noAnimate
      , 1)
    else
      label.removeClass 'topLabel'

  $('.field:not(.noinput)').each ->
    hideLabel(this, true)

  $('.field:not(.noinput)').focusout ->
    hideLabel this

  $('.field:not(.noinput)').focusin ->
    $(this).children('label').addClass('topLabel')
  return