

$(document).on "ready", ->



  counter = $('.itemCounter.counterjs')
  up = counter.find('.countUp')
  down = counter.find('.countDown')
  inp = counter.find('input[type=number]')
  cartTable = $('.cart-open').find('table')
  cartLinkAmount = $('.cart-link .item-amount span')
  counterTable = '<div class="itemCounter counter counterAmount flex">
                  <button class="button smallButton countDown">
                    <i class="fa fa-minus"></i>
                  </button>
                  <span class="number">
                    <input type="number" value="1" step="1" min="1" max="100">
                  </span>
                  <button class="button smallButton countUp">
                    <i class="fa fa-plus"></i>
                  </button>
                </div>'

  counterAmount = '.itemCounter.counterAmount'
  removeLink = '.cart-open table tr .itemRemove'
  animateSpeed = 500
  itemAmount = '.amount span'

  $(counterAmount).each ->
    inp = $(this).find("input[type=number]")
    inp[0].value = inp.data('value')

  sumTotal = ->
    sum = 0
    if cartTable.find('tbody tr').length > 0
      $('.cart-open').addClass('something')
      cartTable.find('tbody tr').each( ->
        sum += parseInt($(this).find('td').eq(5).find('.sum').text(),10)
      )
    else
      $('.cart-open').removeClass('something')
    cartTable.find('tfoot .sumTotal').text(sum)
    return

  counter.each(->
    $(this).find('input').get(0).value = 0
  )

  $.fn.extend({
    sumCount: ->
      this.find('td').eq(5).find('.sum').get(0).innerHTML = this.find(counterAmount).find("input[type=number]")[0].value * this.find('td').eq(3).text()
      $('#item'+this.get(0).id.match(/\d+$/)).find(itemAmount).text(this.find(counterAmount).find("input[type=number]")[0].value)
      sumTotal()
      return this
    changeOriginal: ->
      $('#item'+parseInt(this.get(0).id.match(/\d+$/))).find('.itemCounter.counterjs input[type=number]').val(this.find(counterAmount).find("input[type=number]")[0].value)
      return this
    isNullValue: ->
      c = this.parents('.itemCounter.counterjs').find("input[type=number]")
      return if c[0].value-1 <= parseFloat(c.attr('min')) then true else false
    isNullNow: ->
      c = this.parents('.itemCounter.counterjs').find("input[type=number]")
      return if c[0].value <= parseFloat(c.attr('min')) then true else false
    hideAnimate: (aSpeed) ->
      aSpeed = aSpeed || 300
      this.animate({'opacity':'0'}, aSpeed, ->
        this.remove()
        if cartTable.find('tbody').find('tr').length <= 0
          $('.cart-open').removeClass('something')
      )
      return this
    item: ->
      $this = if $(this).is('tr') then $(this) else $(this).parents('tr')
      id = parseInt($this.get(0).id.match(/\d+$/), 10)
      console.log $('.itemBox#item'+id)
      return $('.itemBox#item'+id)
    amount: ->
      return $(this).find('td').eq(4).find('.itemCounter input').get(0).value
  })



  prepare = () ->
    $this = $(this)
    parent = $this.parents('.itemBox')
    parent.removeClass('noOne')
    arg = []
    arg.push(parseInt(parent[0].id.match(/\d+$/)))
    arg.push(parent.find('img')[0].src) if parent.find('img')[0].src
    arg.push(parent.find('.itemBox_name')[0].text)
    arg.push(parent.find('.itemBox_price').html())
    return arg



  cartAdd = (id, img, name, price) ->
    if !$('.cart-open').hasClass('something')
      $('.cart-open').addClass('something')
    cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) + 1)
    image = '<img src="' + img + '">' if img
    line = '<tr id="trItem'
    line += id + '">'
    line += '<td><div class="itemRemove"><i class="fa fa-times"></i></div></td>'
    line += '<td>'
    line += image  if img
    line += '</td>'
    line += '<td>' + name + '</td>'
    line += '<td>' + price + '</td>'
    line += '<td>' + counterTable + '</td>'
    line += '<td><span class="ruble"><span class="sum"></span><i class="fa fa-rub"></i></span>' + '</td>'
    line += '</tr>'
    cartTable.find('tbody').append(line)
    return cartTable.find('tbody').find('#trItem'+id)




  cartRemove = ->
    $this = $(this)
    parent = $this.parents('.itemBox')
    parent.addClass('noOne')
    cartTable.find('tbody').find('#trItem'+parseInt(parent.get(0).id.match(/\d+$/), 10)).hideAnimate(animateSpeed)
    cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) - 1)
    sumTotal()
    destroy(parent)
    if cartTable.find('tbody').find('tr').length == 0
      $('.cart-open').removeClass('something')
    return


  cartRemoveSimple = ->
    item = $(this).item()
    parent = $(this).parents('tr')
    parent.hideAnimate(animateSpeed)
    if item? && item.length > 0
      origin = item.addClass('noOne').find('.itemCounter.counterjs').find("input[type=number]")
      origin.get(0).value = parseInt(origin.attr('min'), 10)
    cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) - 1)
    destroy(parent)
    setTimeout (->
      sumTotal()
    ),700
    return


  cartRemoveAll = ->
    cartTable.find('tbody').find('tr').each(->
      $(this).hideAnimate(animateSpeed)
      item = $(this).item()
      destroy($(this))
      if item? && item.length > 0
        origin = item.addClass('noOne').find('.itemCounter.counterjs').find("input[type=number]")
        origin.get(0).value = parseInt(origin.attr('min'), 10)
    )
    cartLinkAmount.text(0)
    sumTotal()
    $('.cart-open').removeClass('something')
    return









  # Проверка существования элемента, при существовании увеличи значение этого элемента в поле amount

  elemNoExist = () ->
    $this = $(this)
    parent = $this.parents('.itemBox')
    elem = cartTable.find('#trItem'+parent.get(0).id.match(/\d+$/))
    if elem[0]?
      c = elem.find(counterAmount).find("input[type=number]")
      if $this.hasClass('countUp')
        c.val((index, x) ->
          return if x < parseFloat($(this).attr('max')) then ++x else x
        )
      else
        if $this.hasClass('countDown')
          c.val((index, x) ->
            return if x > parseFloat($(this).attr('min')) then --x else x
          )
        else
          c.val((index, x) ->
            return $this.get(0).value
          )
      elem.sumCount()
      return false
    else
      return true



  send = (elem, count) ->
    return $.ajax
      url: '/menu/order'
      type: 'POST'
      data: {'item_id': parseInt(elem.get(0).id.match(/\d+$/)), "count": count}
      dataType: 'json'
      success: (data) ->
        if $("body").hasClass('production') then yaCounter35349125.reachGoal('add_item')
        return

  sendUpdate = (elem, amount) ->
    return $.ajax
      url: '/menu/order'
      type: 'POST'
      data: {'item_id': parseInt(elem.get(0).id.match(/\d+$/)), "amount": amount}
      dataType: 'json'
      success: (data) ->
        return



  destroy = (elem) ->
    return $.ajax
      url: '/menu/order/delete'
      type: 'DELETE'
      data: {'item_id': parseInt(elem.get(0).id.match(/\d+$/))}
      dataType: 'json'
      success: (data) ->
        return







  $(document).on 'click', counterAmount + ' button', ->
    $(this).parents('tr').sumCount().changeOriginal()
    if $(this).hasClass('countUp')
      send($(this).parents('tr'), '1')
    else
      if $(this).hasClass('countDown')
        send($(this).parents('tr'), '-1')
    return

  $(document).on 'change', counterAmount + ' input', ->
    $(this).parents('tr').sumCount().changeOriginal()
    sendUpdate($(this).parents('tr'), this.value)
    return

  $(document).on 'click', removeLink, ->
    cartRemoveSimple.call(this)
    return

  $(document).on 'click', '.clear-cart', ->
    cartRemoveAll()
    return


  up.on 'click', ->
    if elemNoExist.call(this)
      cartAdd.apply(this, prepare.call(this)).sumCount()
    send($(this).parents('.itemBox'), '1')
    return

  down.on 'click', ->
    if !$(this).isNullNow()
      elemNoExist.call(this)
      if $(this).isNullValue()
        cartRemove.call(this)
      else
        send($(this).parents('.itemBox'), '-1')
    return

  inp.on 'change', ->
    if elemNoExist.call(this)
      cartAdd.apply(this, prepare.call(this)).sumCount()
    if $(this).isNullValue()
      cartRemove.apply(this)
    sendUpdate($(this).parents('.itemBox'), this.value)
    return




















  # events
  pageIsLoad = false

  k = $('.cart-open.something table tbody').find('tr')

  if $('.cart-open').hasClass('something')
    sumTotal()

  k.each ->
    $this = $(this)
    item = $this.item()
    if item.length > 0
      amount = parseInt($this.amount(), 10)
      item.removeClass('noOne').find(itemAmount).text(amount)
      item.find('.itemCounter input').get(0).value = amount
    return


  sumTotal()

  return