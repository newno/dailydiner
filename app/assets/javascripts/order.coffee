
orderAll = '.order-box-all'
orderNext = '.order-next'
orderBack = '.back-step'
boxA = 'box-active'


$(document).on "ready", ->
  if orderAll.length > 0
    checkHeight()
checkHeight = () ->
  $(orderAll).css('height', $(orderAll).find('.'+boxA).css('height'))

goNext = () ->
  $(orderAll).find('.'+boxA).fadeOut(500, ->
    $(this).removeClass(boxA).next().addClass(boxA).fadeIn(500, ->
      checkHeight()
      if $(this).hasClass('detail-box')
        if $("body").hasClass('production') then yaCounter35349125.reachGoal('to-detail-box')
      if $(this).hasClass('pay-box')
        if $("body").hasClass('production') then yaCounter35349125.reachGoal('to-pay-box')
    )
  )
  return
goBack = () ->
  $(orderAll).find('.'+boxA).fadeOut(500, ->
    $(this).removeClass(boxA).prev().addClass(boxA).fadeIn(500, ->
      checkHeight()
    )
  )
  return

$(document).on 'click', orderNext, ->
  goNext()
  false

$(document).on 'click', orderBack, ->
  goBack()
  false

$(document).on 'click', '.return_to_details', (e)->
  e.preventDefault()
  $(this).addClass('active-way')
  return $.ajax
    url: 'order/return'
    type: 'POST'
    dataType: 'json'
    success: (data) ->
      $('.active-way').removeClass('active-way')
      goBack()


$(document).on "ajax:success", '.detail-box form', (e, data, status, xhr) ->
  goNext()

sendPayWay= () ->
  return $.ajax
    url: 'order/payway'
    type: 'POST'
    dataType: 'json'
    success: (data) ->
      $('.active-way').removeClass('active-way')
      modal.modalToggle($('.thanks-box'))

sendPayOnline= () ->
  return $.ajax
    url: 'order/online'
    type: 'POST'
    dataType: 'json'
    success: (data) ->
      formSend(data)


formSend= (data) ->
  form = '<form method="post" action="https://paysto.com/ru/pay/AuthorizeNet" style="display:none;" id="formForSend">'
  for d,v of data
    form += "<input type='text' name='#{d}' value='#{v}' />"
  form += "</form>"
  $(form).appendTo($('.cart-wrapper'))
  $('#formForSend').get(0).submit()
  $('.cart-wrapper').empty()



$(document).on "click", '.pay-buttons-panel a.pay-way-wrap.wallet', ->
  $(this).addClass('active-way')
  sendPayWay()
  if $("body").hasClass('production') then yaCounter35349125.reachGoal('pay-by-cash')
  false


$(document).on "click", '.pay-buttons-panel a.pay-way-wrap.online', ->
  $(this).addClass('active-way')
  sendPayOnline()
  if $("body").hasClass('production') then yaCounter35349125.reachGoal('pay-online')
  false

overlay = '.modal-overlay.order-special'
$(document).on "click", overlay, ->
  console.log 'A'
  document.location.href="/"

$(document).on "click", '.modal-window.order-special .close-button', ->
  console.log 'A'
  document.location.href="/"
