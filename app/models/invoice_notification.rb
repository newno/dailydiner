class InvoiceNotification < ActiveRecord::Base
  # attr_accessible :invoice_id, :pay_data
  belongs_to :invoice, class_name: 'Invoice'
end