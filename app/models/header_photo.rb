class HeaderPhoto < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :se_shop_shop, :class_name => 'SeShop::Shop'
end
