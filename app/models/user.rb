class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :se_shop_items, through: :se_shop_orders
  has_many :se_shop_orders
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_save :admin_create
  def admin_create
    unless User.any?
      self.role_id= 1
    end
  end
end
