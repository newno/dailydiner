class Payment < ActiveRecord::Base
  # attr_accessible :amount,
  #                 :real_amount,
  #                 :gateway_code,
  #                 :gateway_payment_method

  has_one :invoice, class_name: 'Invoice', dependent: :destroy

  validates :amount, presence: true
  validates :amount, exclusion: { in: [0] }, allow_nil: false
end