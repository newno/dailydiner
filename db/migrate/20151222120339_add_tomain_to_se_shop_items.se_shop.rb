# This migration comes from se_shop (originally 20151222120225)
class AddTomainToSeShopItems < ActiveRecord::Migration
  def change
    add_column :se_shop_items, :tomain, :boolean
  end
end
