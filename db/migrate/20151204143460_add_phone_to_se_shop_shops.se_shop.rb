# This migration comes from se_shop (originally 20151204142939)
class AddPhoneToSeShopShops < ActiveRecord::Migration
  def change
    add_column :se_shop_shops, :phone, :string
  end
end
