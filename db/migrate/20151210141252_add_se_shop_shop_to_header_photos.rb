class AddSeShopShopToHeaderPhotos < ActiveRecord::Migration
  def change
    add_reference :header_photos, :se_shop_shop, index: true
  end
end
