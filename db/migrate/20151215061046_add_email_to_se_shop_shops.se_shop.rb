# This migration comes from se_shop (originally 20151215060744)
class AddEmailToSeShopShops < ActiveRecord::Migration
  def change
    add_column :se_shop_shops, :email, :string
  end
end
