# This migration comes from se_shop (originally 20151110081616)
class CreateSeShopShops < ActiveRecord::Migration
  def change
    create_table :se_shop_shops do |t|
      t.string :name
      t.string :city
      t.string :address
      t.string :slug

      t.timestamps null: false
    end
    add_index :se_shop_shops, :slug
  end
end
