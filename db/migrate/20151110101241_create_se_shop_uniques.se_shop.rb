# This migration comes from se_shop (originally 20151110081512)
class CreateSeShopUniques < ActiveRecord::Migration
  def change
    create_table :se_shop_uniques do |t|
      t.belongs_to :item, index: true
      t.belongs_to :shop, index: true
      t.decimal :newprice, :precision=>8, :scale=>2
      t.integer :availability

      t.timestamps null: false
    end
  end
end
