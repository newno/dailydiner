# This migration comes from se_shop (originally 20151110073735)
class CreateSeShopSubcategories < ActiveRecord::Migration
  def change
    create_table :se_shop_subcategories do |t|
      t.string :name
      t.string :image
      t.belongs_to :category, index: true
      t.string :slug

      t.timestamps null: false
    end
    add_index :se_shop_subcategories, :slug
  end
end
