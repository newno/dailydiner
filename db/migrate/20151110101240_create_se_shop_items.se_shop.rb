# This migration comes from se_shop (originally 20151110080149)
class CreateSeShopItems < ActiveRecord::Migration
  def change
    create_table :se_shop_items do |t|
      t.string :name
      t.string :image
      t.decimal :price, :precision=>8, :scale=>2
      t.references :parent, polymorphic: true, index: true
      t.text :shortdescription
      t.text :description
      t.string :slug

      t.timestamps null: false
    end
    add_index :se_shop_items, :slug
  end
end
