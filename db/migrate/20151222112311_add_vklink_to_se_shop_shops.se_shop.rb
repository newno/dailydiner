# This migration comes from se_shop (originally 20151222112117)
class AddVklinkToSeShopShops < ActiveRecord::Migration
  def change
    add_column :se_shop_shops, :vklink, :string
  end
end
