# This migration comes from se_shop (originally 20151207043109)
class CreateSeShopOrderDetails < ActiveRecord::Migration
  def change
    create_table :se_shop_order_details do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :street
      t.string :building
      t.string :apartment
      t.integer :floor
      t.string :porch
      t.text :comment
      t.timestamps null: false
    end
  end
end
