# This migration comes from se_shop (originally 20151127140803)
class AddLogoToSeShopShops < ActiveRecord::Migration
  def change
    add_column :se_shop_shops, :logo, :string
  end
end
