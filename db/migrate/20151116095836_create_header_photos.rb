class CreateHeaderPhotos < ActiveRecord::Migration
  def change
    create_table :header_photos do |t|
      t.string :image
      t.integer :position

      t.timestamps null: false
    end
  end
end
