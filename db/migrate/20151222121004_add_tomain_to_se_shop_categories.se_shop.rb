# This migration comes from se_shop (originally 20151222120856)
class AddTomainToSeShopCategories < ActiveRecord::Migration
  def change
    add_column :se_shop_categories, :tomain, :boolean
  end
end
