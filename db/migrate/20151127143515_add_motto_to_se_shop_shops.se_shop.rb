# This migration comes from se_shop (originally 20151127143429)
class AddMottoToSeShopShops < ActiveRecord::Migration
  def change
    add_column :se_shop_shops, :motto, :string
  end
end
