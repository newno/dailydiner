# This migration comes from se_shop (originally 20151128085555)
class CreateSeShopOrderStatuses < ActiveRecord::Migration
  def change
    create_table :se_shop_order_statuses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
