# This migration comes from se_shop (originally 20151126091652)
class CreateSeShopCategoriesShops < ActiveRecord::Migration
  def change
    create_table :se_shop_categories_shops, id: false do |t|
      t.integer :category_id
      t.integer :shop_id
    end
  end
end
