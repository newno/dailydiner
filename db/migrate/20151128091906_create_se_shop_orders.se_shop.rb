# This migration comes from se_shop (originally 20151128085609)
class CreateSeShopOrders < ActiveRecord::Migration
  def change
    create_table :se_shop_orders do |t|
      t.belongs_to :item, index: true
      t.belongs_to :user, index: true
      t.uuid :user_uuid
      t.integer :amount
      t.belongs_to :order_status, index: true

      t.timestamps null: false
    end
  end
end
