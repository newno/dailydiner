# This migration comes from se_shop (originally 20151207044735)
class AddSeShopOrderDetailToSeShopOrders < ActiveRecord::Migration
  def change
    add_reference :se_shop_orders, :se_shop_order_detail, index: true, foreign_key: true
  end
end
