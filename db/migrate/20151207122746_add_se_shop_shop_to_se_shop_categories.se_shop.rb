# This migration comes from se_shop (originally 20151207110805)
class AddSeShopShopToSeShopCategories < ActiveRecord::Migration
  def change
    add_reference :se_shop_categories, :shop, index: true
  end
end
