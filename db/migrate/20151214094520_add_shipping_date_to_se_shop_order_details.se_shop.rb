# This migration comes from se_shop (originally 20151214093837)
class AddShippingDateToSeShopOrderDetails < ActiveRecord::Migration
  def change
    add_column :se_shop_order_details, :shipping_date, :string
  end
end
