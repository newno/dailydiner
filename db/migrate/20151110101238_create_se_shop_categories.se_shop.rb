# This migration comes from se_shop (originally 20151110073627)
class CreateSeShopCategories < ActiveRecord::Migration
  def change
    create_table :se_shop_categories do |t|
      t.string :name
      t.string :image
      t.string :slug

      t.timestamps null: false
    end
    add_index :se_shop_categories, :slug
  end
end
