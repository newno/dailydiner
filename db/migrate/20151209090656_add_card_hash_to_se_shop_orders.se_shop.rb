# This migration comes from se_shop (originally 20151209085812)
class AddCardHashToSeShopOrders < ActiveRecord::Migration
  def change
    add_column :se_shop_orders, :card_hash, :string
  end
end
