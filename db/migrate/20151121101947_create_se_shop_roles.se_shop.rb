# This migration comes from se_shop (originally 20151121100346)
class CreateSeShopRoles < ActiveRecord::Migration
  def change
    create_table :se_shop_roles do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
