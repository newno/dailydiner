# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
SeShop::Role.create(:name => :admin)
SeShop::Role.create(:name => :shop_admin)
SeShop::OrderStatus.create([{:name => "В корзине"},
                            {:name => "Ждет оплаты"},
                            {:name => "Оплачивается наличными"},
                            {:name => "Оплачен"},
                            {:name => "Подтвержден"},
                            {:name => "Сформирован"},
                            {:name => "Отправлен"},
                            {:name => "Доставлен"}
                           ])
SeShop::Shop.create([
                        {:name => "Daily Diner", :city => "Барнаул",
                         :address => "ул. Анатолия, 68", :motto => "Место, куда хочется возвращаться"},
                        {:name => "Doner Street", :city => "Новоалтайск",
                         :address => "ул. Деповская, 17а", :motto => "Вкусная еда по демократичным ценам"},
                        {:name => "Кофе-бар Корица", :city => "",
                         :address => "", :motto => ""}
                    ])


