# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151229063253) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "header_photos", force: :cascade do |t|
    t.string   "image"
    t.integer  "position"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "se_shop_shop_id"
  end

  add_index "header_photos", ["se_shop_shop_id"], name: "index_header_photos_on_se_shop_shop_id", using: :btree

  create_table "invoice_notifications", force: :cascade do |t|
    t.integer  "invoice_id"
    t.text     "pay_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoice_notifications", ["invoice_id"], name: "index_invoice_notifications_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "payment_id"
    t.float    "amount",     default: 0.0
    t.datetime "paid_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoices", ["payment_id"], name: "index_invoices_on_payment_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.float    "amount",                 default: 0.0
    t.float    "real_amount"
    t.string   "gateway_code"
    t.string   "gateway_payment_method"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "se_shop_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "shop_id"
    t.boolean  "tomain"
  end

  add_index "se_shop_categories", ["shop_id"], name: "index_se_shop_categories_on_shop_id", using: :btree
  add_index "se_shop_categories", ["slug"], name: "index_se_shop_categories_on_slug", using: :btree

  create_table "se_shop_items", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.decimal  "price",            precision: 8, scale: 2
    t.integer  "parent_id"
    t.string   "parent_type"
    t.text     "shortdescription"
    t.text     "description"
    t.string   "slug"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "measurer"
    t.boolean  "tomain"
  end

  add_index "se_shop_items", ["parent_type", "parent_id"], name: "index_se_shop_items_on_parent_type_and_parent_id", using: :btree
  add_index "se_shop_items", ["slug"], name: "index_se_shop_items_on_slug", using: :btree

  create_table "se_shop_order_details", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "street"
    t.string   "building"
    t.string   "apartment"
    t.integer  "floor"
    t.string   "porch"
    t.text     "comment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "shipping_date"
  end

  create_table "se_shop_order_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "se_shop_orders", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "user_id"
    t.uuid     "user_uuid"
    t.integer  "amount"
    t.integer  "order_status_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "se_shop_order_detail_id"
    t.string   "card_hash"
  end

  add_index "se_shop_orders", ["item_id"], name: "index_se_shop_orders_on_item_id", using: :btree
  add_index "se_shop_orders", ["order_status_id"], name: "index_se_shop_orders_on_order_status_id", using: :btree
  add_index "se_shop_orders", ["se_shop_order_detail_id"], name: "index_se_shop_orders_on_se_shop_order_detail_id", using: :btree
  add_index "se_shop_orders", ["user_id"], name: "index_se_shop_orders_on_user_id", using: :btree

  create_table "se_shop_roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "se_shop_shops", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.string   "address"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "logo"
    t.text     "motto"
    t.string   "phone"
    t.string   "email"
    t.string   "vklink"
    t.string   "Instalink"
  end

  add_index "se_shop_shops", ["slug"], name: "index_se_shop_shops_on_slug", using: :btree

  create_table "se_shop_subcategories", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.integer  "category_id"
    t.string   "slug"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "se_shop_subcategories", ["category_id"], name: "index_se_shop_subcategories_on_category_id", using: :btree
  add_index "se_shop_subcategories", ["slug"], name: "index_se_shop_subcategories_on_slug", using: :btree

  create_table "se_shop_uniques", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "shop_id"
    t.decimal  "newprice",     precision: 8, scale: 2
    t.integer  "availability"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "se_shop_uniques", ["item_id"], name: "index_se_shop_uniques_on_item_id", using: :btree
  add_index "se_shop_uniques", ["shop_id"], name: "index_se_shop_uniques_on_shop_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "shop_id"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree
  add_index "users", ["shop_id"], name: "index_users_on_shop_id", using: :btree

  add_foreign_key "se_shop_orders", "se_shop_order_details"
end
