(function() {
  var onScroll;

  $(document).on("page:change", function() {
    $('.aside-scroll').data('offset', $('.aside-scroll').offset().top);
    $(".aside-nav").on("click", "a", function() {
      var target, where;
      target = $(this).attr("href");
      where = $(".container .itemsGrid[data-target='" + target + "']");
      $('html, body').animate({
        scrollTop: where.offset().top - 70
      }, 1000);
      return false;
    });
    return $(document).on('scroll', onScroll);
  });

  onScroll = function(event) {
    var f, nav, navOffset, scrollPos;
    scrollPos = $(document).scrollTop();
    nav = $('.aside-scroll');
    navOffset = nav.data('offset');
    if (navOffset <= scrollPos) {
      nav.addClass('fixed');
      f = nav.outerHeight() + scrollPos;
      if (f > $('.footer').offset().top) {
        nav.css('top', -(f - $('.footer').offset().top));
      } else {
        nav.css('top', 0);
      }
    } else {
      nav.removeClass('fixed');
    }
    nav.find('a').each(function() {
      var currLink, refElement;
      currLink = $(this);
      refElement = $('.itemsGrid[data-target=' + currLink.attr('href') + ']');
      if (refElement.offset().top - 70 <= scrollPos && refElement.offset().top + refElement.height() > scrollPos) {
        $('.aside-nav li a.active').removeClass('active');
        currLink.addClass('active');
      } else {
        currLink.removeClass('active');
      }
    });
  };

}).call(this);
