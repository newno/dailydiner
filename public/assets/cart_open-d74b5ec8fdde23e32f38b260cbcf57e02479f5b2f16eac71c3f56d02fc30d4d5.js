(function() {
  $(document).on("page:change", function() {
    var boxA, cart, cartToggle, checkHeight, orderAll, orderNext, overlay;
    cart = $('.cart-open');
    overlay = $('.overlay-cart');
    orderAll = $('.order-box-all');
    orderNext = $('.order-next');
    boxA = 'box-active';
    cartToggle = function(s1, s2) {
      s1 = s1 || 500;
      s2 = s2 || s1;
      overlay.animate({
        'opacity': 'toggle'
      }, s1);
      return cart.animate({
        'opacity': 'toggle'
      }, s2);
    };
    $('.cart-link').on("click", function() {
      cartToggle.call(400, 800);
      return false;
    });
    overlay.on("click", function() {
      cartToggle.call(800, 400);
      return false;
    });
    $(document).on('click', '.back-to-shop:not(".simple-link")', function() {
      cartToggle.call(800, 400);
      return false;
    });
    checkHeight = function() {
      return orderAll.css('height', orderAll.find('.' + boxA).css('height'));
    };
    if (orderAll.length > 0) {
      checkHeight();
    }
    orderNext.on('click', function() {
      orderAll.find('.' + boxA).fadeOut(500, function() {
        return $(this).removeClass(boxA).next().addClass(boxA).fadeIn(500, function() {
          return checkHeight();
        });
      });
      return false;
    });
    return $(".detail-box form").on("ajax:success", function(e, data, status, xhr) {
      return alert("Спасибо за заказ");
    });
  });

}).call(this);
