(function() {
  var onScroll;

  $(document).on("page:change", function() {
    var cart, cartOffset, left, parent;
    if ($('.menuContainer').length > 0) {
      cart = $('.cart-link');
      cart.data('top', cart.css('top'));
      cartOffset = $('.menuContainer').offset().top;
      $('.menuContainer').data('offset', cartOffset);
      $(document).on('scroll', onScroll);
      $('body').data('wh', $(window).height() / 2);
    }
    parent = $('.emblemBlock .lenta');
    if (parent.length > 0) {
      left = parent.find('.active').get(0).offsetLeft + parent.find('.active').width() / 2;
      parent.css('margin-left', -left);
    }
    return $('.emblemBlock .lenta').on('click', 'a.active', function() {
      return false;
    });
  });

  onScroll = function(event) {
    var $wH, cart, cartOffset, cartTop, scrollPos;
    scrollPos = $(document).scrollTop();
    $wH = $('body').data('wh');
    cartOffset = $('.menuContainer').data('offset');
    cart = $('.cart-link');
    cartTop = cart.data('top');
    if (cartOffset - $wH >= scrollPos) {
      cart.addClass('stop');
    } else {
      cart.removeClass('stop');
    }
  };

}).call(this);
