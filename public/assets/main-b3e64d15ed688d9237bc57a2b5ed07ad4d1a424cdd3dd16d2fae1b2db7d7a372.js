(function() {
  var onScroll;

  $(document).on("page:change", function() {
    $('.setSize').each(function() {
      return $(this).setSize();
    });
    $(window).load(function() {
      return $('body').addClass('ready');
    });
    if ($(".aside-nav").length > 0) {
      $('.aside-scroll').data('offset', $('.aside-scroll').offset().top);
      $(".aside-nav").on("click", "a", function() {
        var target, where;
        target = $(this).attr("href");
        where = $(".container .itemsGrid[data-target='" + target + "']");
        $('html, body').animate({
          scrollTop: where.offset().top - 70
        }, 1000);
        return false;
      });
      return $(document).on('scroll', onScroll);
    }
  });

  $(document).on("click", "button.countDown", function() {
    $(this).parents(".itemCounter").find("input[type=number]").val(function(index, x) {
      if (x > parseFloat($(this).attr('min'))) {
        return --x;
      } else {
        return x;
      }
    });
    return false;
  });

  $(document).on("click", "button.countUp", function() {
    $(this).parents(".itemCounter").find("input[type=number]").val(function(index, x) {
      if (x < parseFloat($(this).attr('max'))) {
        return ++x;
      } else {
        return x;
      }
    });
    return false;
  });

  onScroll = function(event) {
    var f, nav, navOffset, scrollPos;
    scrollPos = $(document).scrollTop();
    nav = $('.aside-scroll');
    navOffset = nav.data('offset');
    if (navOffset <= scrollPos) {
      nav.addClass('fixed');
      f = nav.outerHeight() + scrollPos;
      if (f > $('.footer').offset().top) {
        nav.css('top', -(f - $('.footer').offset().top));
      } else {
        nav.css('top', 0);
      }
    } else {
      nav.removeClass('fixed');
    }
    nav.find('a').each(function() {
      var currLink, refElement;
      currLink = $(this);
      refElement = $('.itemsGrid[data-target=' + currLink.attr('href') + ']');
      if (refElement.offset().top - 70 <= scrollPos && refElement.offset().top + refElement.height() > scrollPos) {
        $('.aside-nav li a.active').removeClass('active');
        currLink.addClass('active');
      } else {
        currLink.removeClass('active');
      }
    });
  };

}).call(this);
