(function() {
  this.orderPoller = {
    poll: function() {
      return setTimeout(this.request, 10000);
    },
    request: function() {
      if ($('#orderReady').length > 0) {
        return $.get($('#orderReady').data('url'), {
          after: $('#orderReady .order-ready').last().get(0).id
        });
      }
    },
    addOrder: function(elem) {
      if ($(elem).length > 0) {
        $('#orders #order-body').append($(elem).addClass('new-order'));
        $('#notification-sound').get(0).play();
      }
      return this.poll();
    },
    setStatus: function(elem, status, order_id) {
      return $.ajax({
        url: '/menu/admin/order_status',
        type: 'POST',
        data: {
          'status_id': status,
          "order_id": order_id
        },
        dataType: 'json',
        success: function(data) {
          return orderPoller.refreshStatus(elem, data.newStatusId, data.newStatusName);
        }
      });
    },
    refreshStatus: function(elem, id, name) {
      var oldname, parent;
      oldname = $(elem).data('name');
      parent = $(elem).parents('tbody.order-master');
      parent.addClass('status' + (id - 1));
      $(elem).parents('.manage-column').find('.statusNow').text(oldname);
      if (id <= $('#order-body').data('max-status')) {
        $(elem).data({
          'status': id,
          'name': name
        }).text(name);
      } else {
        $(elem).fadeOut(500, function() {
          $(this).remove();
          setTimeout((function() {
            return parent.animate({
              'opacity': 0,
              'left': '-=500'
            }, 300, function() {
              return $(this).remove();
            });
          }), 5000);
          return false;
        });
      }
      return false;
    }
  };

  $(document).on("page:change", function() {
    if ($('#orders').length > 0) {
      orderPoller.poll();
    }
  });

  $(document).on("click", "a.setStatus", function() {
    if ($(this).data('status') <= $('#order-body').data('max-status')) {
      orderPoller.setStatus($(this), $(this).data('status'), $(this).data('order-id'));
    }
    return false;
  });

}).call(this);
