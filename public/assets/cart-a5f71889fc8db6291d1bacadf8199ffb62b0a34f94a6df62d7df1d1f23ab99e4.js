(function() {
  $(document).on("page:change", function() {
    var animateSpeed, cartAdd, cartLinkAmount, cartRemove, cartRemoveAll, cartRemoveSimple, cartTable, counter, counterAmount, counterTable, destroy, down, elemNoExist, inp, itemAmount, k, prepare, removeLink, send, sendUpdate, sumTotal, up;
    counter = $('.itemCounter.counterjs');
    up = counter.find('.countUp');
    down = counter.find('.countDown');
    inp = counter.find('input[type=number]');
    cartTable = $('.cart-open').find('table');
    cartLinkAmount = $('.cart-link .item-amount span');
    counterTable = '<div class="itemCounter counter counterAmount flex"> <button class="button smallButton countDown"> <i class="fa fa-minus"></i> </button> <span class="number"> <input type="number" value="1" step="1" min="1" max="100"> </span> <button class="button smallButton countUp"> <i class="fa fa-plus"></i> </button> </div>';
    counterAmount = '.itemCounter.counterAmount';
    removeLink = '.cart-open table tr .itemRemove';
    animateSpeed = 500;
    itemAmount = '.amount span';
    counter.each(function() {
      return $(this).find('input').get(0).value = 0;
    });
    prepare = function() {
      var $this, arg, parent;
      $this = $(this);
      parent = $this.parents('.itemBox');
      parent.removeClass('noOne');
      arg = [];
      arg.push(parseInt(parent[0].id.match(/\d+$/)));
      arg.push(parent.find('img')[0].src);
      arg.push(parent.find('.itemBox_name')[0].text);
      arg.push(parent.find('.itemBox_price').html());
      return arg;
    };
    cartAdd = function(id, img, name, price) {
      var image, line;
      if (!$('.cart-open').hasClass('something')) {
        $('.cart-open').addClass('something');
      }
      cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) + 1);
      if (img) {
        image = '<img src="' + img + '">';
      }
      line = '<tr id="trItem';
      line += id + '">';
      line += '<td><div class="itemRemove"><i class="fa fa-times"></i></div></td>';
      line += '<td>';
      if (img) {
        line += image;
      }
      line += '</td>';
      line += '<td>' + name + '</td>';
      line += '<td>' + price + '</td>';
      line += '<td>' + counterTable + '</td>';
      line += '<td><span class="ruble"><span class="sum"></span><i class="fa fa-rub"></i></span>' + '</td>';
      line += '</tr>';
      cartTable.find('tbody').append(line);
      return cartTable.find('tbody').find('#trItem' + id);
    };
    cartRemove = function() {
      var $this, parent;
      $this = $(this);
      parent = $this.parents('.itemBox');
      parent.addClass('noOne');
      cartTable.find('tbody').find('#trItem' + parseInt(parent.get(0).id.match(/\d+$/), 10)).hideAnimate(animateSpeed);
      cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) - 1);
      sumTotal();
      destroy(parent);
      if (cartTable.find('tbody').find('tr').length === 0) {
        return $('.cart-open').removeClass('something');
      }
    };
    cartRemoveSimple = function() {
      var item, origin, parent;
      item = $(this).item();
      parent = $(this).parents('tr');
      parent.hideAnimate(animateSpeed);
      if ((item != null) && item.length > 0) {
        origin = item.addClass('noOne').find('.itemCounter.counterjs').find("input[type=number]");
        origin.get(0).value = parseInt(origin.attr('min'), 10);
      }
      cartLinkAmount.text(parseInt(cartLinkAmount.text(), 10) - 1);
      destroy(parent);
      if (cartTable.find('tbody').find('tr').length === 0) {
        return $('.cart-open').removeClass('something');
      }
    };
    cartRemoveAll = function() {
      var $this;
      $this = $(this);
      cartTable.find('tbody').find('tr').each(function() {
        var item, origin;
        $(this).hideAnimate(animateSpeed);
        item = $(this).item();
        destroy($(this));
        if ((item != null) && item.length > 0) {
          origin = item.addClass('noOne').find('.itemCounter.counterjs').find("input[type=number]");
          return origin.get(0).value = parseInt(origin.attr('min'), 10);
        }
      });
      cartLinkAmount.text(0);
      sumTotal();
      return $('.cart-open').removeClass('something');
    };
    sumTotal = function() {
      var sum;
      sum = 0;
      if (cartTable.find('tbody tr').length) {
        $('.cart-open').addClass('something');
        cartTable.find('tbody tr').each(function() {
          return sum += parseInt($(this).find('td').eq(5).find('.sum').text(), 10);
        });
      } else {
        $('.cart-open').removeClass('something');
      }
      return cartTable.find('tfoot .sumTotal').text(sum);
    };
    $.fn.extend({
      sumCount: function() {
        this.find('td').eq(5).find('.sum').get(0).innerHTML = this.find(counterAmount).find("input[type=number]")[0].value * this.find('td').eq(3).text();
        $('#item' + this.get(0).id.match(/\d+$/)).find(itemAmount).text(this.find(counterAmount).find("input[type=number]")[0].value);
        sumTotal();
        return this;
      },
      changeOriginal: function() {
        $('#item' + parseInt(this.get(0).id.match(/\d+$/))).find('.itemCounter.counterjs input[type=number]').val(this.find(counterAmount).find("input[type=number]")[0].value);
        return this;
      },
      isNullValue: function() {
        var c;
        c = this.parents('.itemCounter.counterjs').find("input[type=number]");
        if (c[0].value - 1 <= parseFloat(c.attr('min'))) {
          return true;
        } else {
          return false;
        }
      },
      hideAnimate: function(aSpeed) {
        aSpeed = aSpeed || 300;
        this.animate({
          'opacity': '0'
        }, aSpeed, function() {
          return this.remove();
        });
        return this;
      },
      item: function() {
        var $this, id;
        $this = $(this).is('tr') ? $(this) : $(this).parents('tr');
        id = parseInt($this.get(0).id.match(/\d+$/), 10);
        return $('.itemBox#item' + id);
      },
      amount: function() {
        return $(this).find('td').eq(4).find('.itemCounter input').get(0).value;
      }
    });
    elemNoExist = function() {
      var $this, c, elem, parent;
      $this = $(this);
      parent = $this.parents('.itemBox');
      elem = cartTable.find('#trItem' + parent.get(0).id.match(/\d+$/));
      if (elem[0] != null) {
        c = elem.find(counterAmount).find("input[type=number]");
        if ($this.hasClass('countUp')) {
          c.val(function(index, x) {
            if (x < parseFloat($(this).attr('max'))) {
              return ++x;
            } else {
              return x;
            }
          });
        } else {
          if ($this.hasClass('countDown')) {
            c.val(function(index, x) {
              if (x > parseFloat($(this).attr('min'))) {
                return --x;
              } else {
                return x;
              }
            });
          } else {
            c.val(function(index, x) {
              return $this.get(0).value;
            });
          }
        }
        elem.sumCount();
        return false;
      } else {
        return true;
      }
    };
    send = function(elem, count) {
      return $.ajax({
        url: '/menu/order',
        type: 'POST',
        data: {
          'item_id': parseInt(elem.get(0).id.match(/\d+$/)),
          "count": count
        },
        dataType: 'json',
        success: function(data) {
          return false;
        }
      });
    };
    sendUpdate = function(elem, amount) {
      return $.ajax({
        url: '/menu/order',
        type: 'POST',
        data: {
          'item_id': parseInt(elem.get(0).id.match(/\d+$/)),
          "amount": amount
        },
        dataType: 'json',
        success: function(data) {
          return false;
        }
      });
    };
    destroy = function(elem) {
      return $.ajax({
        url: '/menu/order/delete',
        type: 'DELETE',
        data: {
          'item_id': parseInt(elem.get(0).id.match(/\d+$/))
        },
        dataType: 'json',
        success: function(data) {
          return false;
        }
      });
    };
    k = $('.cart-open.something table tbody').find('tr');
    if ($('.cart-open').hasClass('something')) {
      cartLinkAmount.text(parseInt(k.length));
      sumTotal();
    }
    k.each(function() {
      var $this, amount, item;
      $this = $(this);
      item = $this.item();
      if ((item != null) && item.length > 0) {
        amount = parseInt($this.amount(), 10);
        item.removeClass('noOne').find(itemAmount).text(amount);
        return item.find('.itemCounter input').get(0).value = amount;
      }
    });
    sumTotal();
    $(document).on('click', counterAmount + ' button', function() {
      $(this).parents('tr').sumCount().changeOriginal();
      if ($(this).hasClass('countUp')) {
        return send($(this).parents('tr'), '1');
      } else {
        if ($(this).hasClass('countDown')) {
          return send($(this).parents('tr'), '-1');
        }
      }
    });
    $(document).on('change', counterAmount + ' input', function() {
      $(this).parents('tr').sumCount().changeOriginal();
      return sendUpdate($(this).parents('tr'), this.value);
    });
    $(document).on('click', removeLink, function() {
      return cartRemoveSimple.apply(this);
    });
    $(document).on('click', '.clear-cart', function() {
      return cartRemoveAll.apply(this);
    });
    up.on('click', function() {
      if (elemNoExist.call(this)) {
        cartAdd.apply(this, prepare.call(this)).sumCount();
      }
      return send($(this).parents('.itemBox'), '1');
    });
    down.on('click', function() {
      elemNoExist.call(this);
      if ($(this).isNullValue()) {
        return cartRemove.apply(this);
      } else {
        return send($(this).parents('.itemBox'), '-1');
      }
    });
    return inp.on('change', function() {
      if (elemNoExist.call(this)) {
        cartAdd.apply(this, prepare.call(this)).sumCount();
      }
      if ($(this).isNullValue()) {
        cartRemove.apply(this);
      }
      return sendUpdate($(this).parents('.itemBox'), this.value);
    });
  });

}).call(this);
