(function() {
  $(document).on("page:change", function() {
    var cart, overlay;
    cart = $('.cart-open');
    overlay = $('.overlay-cart');
    $('.cart-link').on("click", function() {
      modal.modalToggle(cart, 400, 800);
      return false;
    });
    return $(document).on('click', '.back-to-shop:not(".simple-link")', function() {
      modal.modalToggle(this, 800, 400);
      return false;
    });
  });

}).call(this);
