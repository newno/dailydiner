(function() {
  $(document).on("page:change", function() {
    var footer, geocodeAddress, geocoder, setMap;
    setMap = function(latlng) {
      var map, marker;
      map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        scrollwheel: false,
        zoom: 17
      });
      marker = new google.maps.Marker({
        map: map,
        position: latlng,
        title: 'Hello World!'
      });
    };
    geocodeAddress = function(geocoder, address) {
      return geocoder.geocode({
        'address': address
      }, function(results, status) {
        setMap(results[0].geometry.location);
      });
    };
    geocoder = new google.maps.Geocoder();
    if ($('#map').length > 0) {
      $('.main').css('margin-bottom', 0);
      geocodeAddress(geocoder, $('.emblemBlock a.active').data('address'));
    }
    footer = $('.contactContainer');
    $(document).on('click', '.emblemBlock a', function() {
      var address, cl, footerAttr, info;
      address = $(this).data('address');
      console.log(address[0]);
      if ((address[0] != null) && address.length > 15) {
        geocodeAddress(geocoder, address);
        footerAttr = {
          phone: $(this).data("phone"),
          street: $(this).data("street"),
          email: $(this).data("email")
        };
        footer.find('.phoneSpace span').text(footerAttr.phone.slice(footerAttr.phone.indexOf(")") + 1));
        footer.find('.phoneSpace span.small-text').text(footerAttr.phone.slice(0, footerAttr.phone.indexOf(")") + 1));
        footer.find('.address').text(footerAttr.street);
        footer.find('.email').text(footerAttr.email);
        $('.emblemBlock a.active').removeClass('active');
        $(this).addClass('active');
        cl = 'exist';
        info = '.map-box .shop-info';
        $(info + '.' + cl).removeClass(cl);
        $(info).eq($(this).index()).addClass(cl);
      }
      return false;
    });
  });

}).call(this);
