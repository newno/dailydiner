(function() {
  $(document).on("page:change", function() {
    var hideLabel;
    hideLabel = function(field, noAnimate) {
      var inp, label;
      noAnimate = noAnimate || false;
      inp = $(field).children('input, textarea');
      label = $(field).children('label');
      if (inp.val()) {
        if (noAnimate) {
          label.addClass('topLabelNOAnimate');
        }
        return setTimeout(function() {
          label.addClass('topLabel');
          if (noAnimate) {
            return label.removeClass('topLabelNOAnimate');
          }
        }, 1);
      } else {
        return label.removeClass('topLabel');
      }
    };
    $('.field:not(.noinput)').each(function() {
      return hideLabel(this, true);
    });
    $('.field:not(.noinput)').focusout(function() {
      return hideLabel(this);
    });
    $('.field:not(.noinput)').focusin(function() {
      return $(this).children('label').addClass('topLabel');
    });
  });

}).call(this);
