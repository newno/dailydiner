(function() {
  this.orderPoller = {
    poll: function() {
      return setTimeout(this.request, 10000);
    },
    request: function() {
      if ($('#orderReady').length > 0) {
        return $.get($('#orderReady').data('url'), {
          after: $('#orderReady .order-ready').last().get(0).id
        });
      }
    },
    addOrder: function(elem) {
      if ($(elem).length > 0) {
        $('#orders #order-body').append($(elem).addClass('new-order'));
        this.sound();
      }
      return this.poll();
    },
    sound: function() {
      var s, stop, t, timerId;
      s = $('#notification-sound').get(0);
      s.play();
      t = 3000;
      stop = t * 4 + t / 2;
      timerId = setInterval((function() {
        s.play();
      }), t);
      return setTimeout((function() {
        clearInterval(timerId);
      }), stop);
    },
    setStatus: function(elem, status, order_id) {
      return $.ajax({
        url: '/menu/admin/order_status',
        type: 'POST',
        data: {
          'status_id': status,
          "order_id": order_id
        },
        dataType: 'json',
        success: function(data) {
          return orderPoller.refreshStatus(elem, data.newStatusId, data.newStatusName);
        }
      });
    },
    destroyOrder: function(elem, id) {
      var parent;
      parent = $(elem).parents('tbody.order-master');
      return $.ajax({
        url: '/menu/admin/order_destroy',
        type: 'POST',
        data: {
          "order_id": id
        },
        dataType: 'json',
        success: function(data) {
          return parent.fadeOut(500, function() {
            setTimeout((function() {
              return parent.animate({
                'opacity': 0,
                'left': '-=500'
              }, 300, function() {
                return $(this).remove();
              });
            }), 300);
            return false;
          });
        }
      });
    },
    refreshStatus: function(elem, id, name) {
      var oldname, parent;
      oldname = $(elem).data('name');
      parent = $(elem).parents('tbody.order-master');
      parent.addClass('status' + (id - 1));
      $(elem).parents('.manage-column').find('.statusNow').text(oldname);
      if (id <= $('#order-body').data('max-status')) {
        console.log(id);
        if (id <= 6) {
          parent.find('a.destroy').animate({
            'opacity': 0
          }, 300, function() {
            return $(this).remove();
          });
        }
        $(elem).data({
          'status': id,
          'name': name
        }).text(name);
      } else {
        $(elem).fadeOut(500, function() {
          $(this).remove();
          setTimeout((function() {
            return parent.animate({
              'opacity': 0,
              'left': '-=500'
            }, 300, function() {
              return $(this).remove();
            });
          }), 5000);
          return false;
        });
      }
      return false;
    }
  };

  $(document).on("page:change", function() {
    if ($('#orders').length > 0) {
      orderPoller.poll();
    }
  });

  $(document).on("click", "a.setStatus", function() {
    if ($(this).data('status') <= $('#order-body').data('max-status')) {
      orderPoller.setStatus($(this), $(this).data('status'), $(this).data('order-id'));
    }
    return false;
  });

  $(document).on("click", "a.destroy", function() {
    orderPoller.destroyOrder($(this), $(this).data('order-id'));
    return false;
  });

}).call(this);
