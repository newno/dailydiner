(function() {
  $(document).on("page:change", function() {
    var cart, cartToggle, overlay;
    cart = $('.cart-open');
    overlay = $('.overlay-cart');
    cartToggle = function(s1, s2) {
      s1 = s1 || 500;
      s2 = s2 || s1;
      overlay.animate({
        'opacity': 'toggle'
      }, s1);
      return cart.animate({
        'opacity': 'toggle'
      }, s2);
    };
    $('.cart-link').on("click", function() {
      cartToggle.call(400, 800);
      return false;
    });
    overlay.on("click", function() {
      cartToggle.call(800, 400);
      return false;
    });
    return $(document).on('click', '.back-to-shop:not(".simple-link")', function() {
      cartToggle.call(800, 400);
      return false;
    });
  });

}).call(this);
