(function() {
  (function($) {
    return $.fn.setSize = function() {
      var $this, $width, bg, getType, image, loadimg, mobile, pos, size, type;
      $this = $(this);
      $this.addClass('loading');
      size = $this.data('size' || false);
      image = $this.data('setimage' || false);
      mobile = $(window).width() > 700 ? false : $this.data('mobile' || false);
      if (!$(this).data('setSize')) {
        $width = $(window).width();
        if (size) {
          $width = $width / size;
        }
        getType = function() {
          var $type;
          $type = '';
          if ($width < 400) {
            $type = 'w400_';
          } else if ($width < 700) {
            $type = 'w700_';
          } else if ($width < 1000) {
            $type = 'w1000_';
          } else if ($width < 1500) {
            $type = 'w1500_';
          } else if ($width < 2000) {
            $type = 'w2000_';
          } else {
            $type = '';
          }
          return $type;
        };
        bg = $this.attr('data-bg');
        if (bg != null) {
          type = getType.call($width);
          pos = bg.lastIndexOf('/') + 1;
          bg = bg.slice(0, pos) + type + bg.slice(pos);
          if (!mobile) {
            if (image) {
              $this.removeAttr('data-bg').data('setSize', true).attr('src', bg).load(function() {
                if ($(this).width() <= $(this).height()) {
                  $this.addClass('portain');
                }
                if ($(this).width() * 3 <= $(this).height()) {
                  $this.addClass('soLong');
                }
                return $this.removeClass('loading');
              });
            } else {
              loadimg = $(new Image()).attr({
                'src': bg,
                'style': "position:absolute; opacity: 0;"
              });
              bg = "url(" + bg + ")";
              loadimg.load(function() {
                $(this).remove();
                return $this.removeClass('loading').removeAttr('data-bg').data('setSize', true).css('background-image', bg);
              });
            }
          }
        }
      }
      return $this;
    };
  })(jQuery);

}).call(this);
