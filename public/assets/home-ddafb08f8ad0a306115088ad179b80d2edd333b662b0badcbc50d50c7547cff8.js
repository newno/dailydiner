(function() {
  var onScroll;

  $(document).on("page:change", function() {
    var cart, cartOffset;
    cart = $('.cart-link');
    cart.data('top', cart.css('top'));
    cartOffset = $('.menuContainer').offset().top;
    cart.css({
      'position': 'absolute',
      "top": cartOffset
    });
    console.log(cartOffset);
    return $(document).on('scroll', onScroll);
  });

  onScroll = function(event) {
    var cart, cartOffset, cartTop, scrollPos;
    scrollPos = $(document).scrollTop();
    cartOffset = $('.menuContainer').offset().top;
    cart = $('.cart-link');
    cartTop = cart.data('top');
    if (cartOffset <= scrollPos) {
      cart.css({
        'position': 'absolute',
        "top": cartOffset
      });
    } else {
      cart.css({
        'position': 'fixed',
        "top": cartTop
      });
    }
  };

}).call(this);
