(function() {
  var modalWindow, overlay;

  modalWindow = ".modal-window";

  overlay = '.modal-overlay';

  this.modal = {
    modalToggle: function(elem, s1, s2) {
      var $modal;
      s1 = s1 || 500;
      s2 = s2 || s1;
      $modal = elem[0] != null ? $(elem).parents(modalWindow) : $(modalWindow).not(":hidden");
      $(overlay).animate({
        'opacity': 'toggle'
      }, s1);
      return $modal.animate({
        'opacity': 'toggle'
      }, s2);
    }
  };

  $(document).on('page:change', function() {
    $(overlay).on("click", function() {
      modal.modalToggle(800, 400);
      return false;
    });
    return $('.modal-window .close-button').on("click", function() {
      modal.modalToggle(800, 400);
      return false;
    });
  });

}).call(this);
