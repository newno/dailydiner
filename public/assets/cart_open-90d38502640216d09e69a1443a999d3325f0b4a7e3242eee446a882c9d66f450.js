(function() {
  $(document).on("page:change", function() {
    var boxA, cart, checkHeight, orderAll, orderNext, overlay;
    cart = $('.cart-open');
    overlay = $('.overlay-cart');
    orderAll = $('.order-box-all');
    orderNext = $('.order-next');
    boxA = 'box-active';
    $('.cart-link').on("click", function() {
      modal.modalToggle(cart, 400, 800);
      return false;
    });
    $(document).on('click', '.back-to-shop:not(".simple-link")', function() {
      modal.modalToggle(this, 800, 400);
      return false;
    });
    checkHeight = function() {
      return orderAll.css('height', orderAll.find('.' + boxA).css('height'));
    };
    if (orderAll.length > 0) {
      checkHeight();
    }
    orderNext.on('click', function() {
      orderAll.find('.' + boxA).fadeOut(500, function() {
        return $(this).removeClass(boxA).next().addClass(boxA).fadeIn(500, function() {
          return checkHeight();
        });
      });
      return false;
    });
    return $(".detail-box form").on("ajax:success", function(e, data, status, xhr) {
      return alert("Спасибо за заказ");
    });
  });

}).call(this);
