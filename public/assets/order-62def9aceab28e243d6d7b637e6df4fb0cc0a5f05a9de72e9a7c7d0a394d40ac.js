(function() {
  $(document).on("page:change", function() {
    var boxA, checkHeight, goNext, orderAll, orderNext, overlay, sendPayWay;
    orderAll = $('.order-box-all');
    orderNext = $('.order-next');
    boxA = 'box-active';
    checkHeight = function() {
      return orderAll.css('height', orderAll.find('.' + boxA).css('height'));
    };
    if (orderAll.length > 0) {
      checkHeight();
    }
    goNext = function() {
      return orderAll.find('.' + boxA).fadeOut(500, function() {
        return $(this).removeClass(boxA).next().addClass(boxA).fadeIn(500, function() {
          return checkHeight();
        });
      });
    };
    orderNext.on('click', function() {
      goNext();
      return false;
    });
    $(".detail-box form").on("ajax:success", function(e, data, status, xhr) {
      return goNext();
    });
    sendPayWay = function() {
      return $.ajax({
        url: 'order/payway',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
          $('.active-way').removeClass('active-way');
          return modal.modalToggle($('.thanks-box'));
        }
      });
    };
    $('.pay-buttons-panel a.pay-way-wrap.wallet').on("click", function() {
      $(this).addClass('active-way');
      sendPayWay();
      return false;
    });
    overlay = '.modal-overlay.order-special';
    $(overlay).on("click", function() {
      document.location.href = "/";
      return false;
    });
    return $('.modal-window.order-special .close-button').on("click", function() {
      document.location.href = "/";
      return false;
    });
  });

}).call(this);
