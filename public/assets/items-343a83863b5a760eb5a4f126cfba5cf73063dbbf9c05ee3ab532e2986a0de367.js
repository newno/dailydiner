(function() {
  $(document).on("page:change", function() {
    var item, itemOpenToggle, overlay;
    item = $('.item-open');
    overlay = $('.overlay-item');
    itemOpenToggle = function(s1, s2) {
      s1 = s1 || 500;
      s2 = s2 || s1;
      overlay.animate({
        'opacity': 'toggle'
      }, s1);
      return item.animate({
        'opacity': 'toggle'
      }, s2);
    };
    overlay.on("click", function() {
      itemOpenToggle.call(800, 400);
      return false;
    });
    $(".itemBox   .itemBox_name").on("ajax:complete", function(e, data, status, xhr) {
      item.empty().append(data.responseText);
      item.find('.setSize').setSize();
      return itemOpenToggle();
    });
  });

}).call(this);
