(function() {
  jQuery.fn.outerHTML = function(s) {
    if (s) {
      return this.before(s).remove();
    } else {
      return jQuery('<p>').append(this.eq(0).clone()).html();
    }
  };

  $(document).on("page:change", function() {
    var addToHead, container, send, setting;
    setting = {
      revert: true,
      start: function() {
        return $(this).addClass('dragActive');
      },
      stop: function() {
        return $(this).removeClass('dragActive');
      },
      containment: ".main"
    };
    container = $('.photos-container');
    send = function(elem, position) {
      return $.ajax({
        url: '/photos/update',
        type: 'PATCH',
        data: {
          'id': parseInt(elem.get(0).id.match(/\d+$/)),
          "position": position
        },
        dataType: 'json',
        success: function(data) {
          return false;
        }
      });
    };
    addToHead = function(elem, target) {
      var $this, item, newold, old;
      $this = elem;
      item = $this.find('.setSize');
      old = target.find('.setSize');
      newold = '<div class="photo draggable" id="';
      newold += target.get(0).id;
      newold += '"><a method="delete" href="/photos/';
      newold += target.get(0).id.match(/\d+$/);
      newold += '" data-remote="true" class="fa fa-times delete-link"></a><div class="photo-overlay i-item"><i class="fa fa-hand-rock-o"></i></div>';
      newold += old.outerHTML();
      newold += "</div>";
      if (old.length > 0) {
        $(newold).prependTo(container).draggable(setting);
        old.remove();
      }
      target.get(0).id = $this.get(0).id;
      target.append(item);
      send($this, target.index() + 1);
      $this.remove();
    };
    $('.draggable').draggable(setting);
    $('.header-photos .photo-box').droppable({
      drop: function(event, ui) {
        addToHead(ui.draggable, $(this));
      },
      hoverClass: 'hover',
      tolerance: 'fit'
    });
  });

}).call(this);
