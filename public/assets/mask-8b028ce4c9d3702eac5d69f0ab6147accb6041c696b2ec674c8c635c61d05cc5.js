(function() {
  $(document).on("page:change", function() {
    $('input.phone').inputmask({
      mask: "+7(999)999-99-99"
    });
    $('input.phone_shop').inputmask({
      mask: "+7(9999)99-99-99"
    });
    return $('input.time').inputmask({
      mask: "99:99"
    });
  });

}).call(this);
