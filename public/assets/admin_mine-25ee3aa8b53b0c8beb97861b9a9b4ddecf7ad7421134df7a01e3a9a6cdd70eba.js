(function() {
  var updateOrders;

  updateOrders = function() {
    $.getScript('/seshop/admin/main/orderready.js');
    setTimeout(updateOrders, 1000);
  };

  $(document).on("page:change", function() {
    setTimeout(updateOrders, 1000);
  });

}).call(this);
