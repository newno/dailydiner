(function() {
  $(document).on("page:change", function() {
    $('#new_order_detail').validate({
      errorPlacement: function(error, element) {
        if ($(element).next(".error-box").length <= 0) {
          $(element).parent().append("<div class='error-box'></div>");
        }
        return $(element).next(".error-box").empty().append(error.text());
      },
      rules: {
        'order_detail[name]': {
          required: true,
          rangelength: [2, 30]
        },
        'order_detail[street]': {
          required: true,
          rangelength: [2, 30]
        },
        'order_detail[phone]': {
          required: true
        },
        'order_detail[building]': {
          required: true,
          rangelength: [1, 5]
        },
        'order_detail[email]': {
          email: true
        }
      }
    });
  });

}).call(this);
