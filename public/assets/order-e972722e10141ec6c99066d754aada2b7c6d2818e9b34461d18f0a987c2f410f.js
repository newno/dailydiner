(function() {
  $(document).on("page:change", function() {
    var boxA, checkHeight, formSend, goBack, goNext, orderAll, orderBack, orderNext, overlay, sendPayOnline, sendPayWay;
    orderAll = $('.order-box-all');
    orderNext = $('.order-next');
    orderBack = $('.back-step');
    boxA = 'box-active';
    checkHeight = function() {
      return orderAll.css('height', orderAll.find('.' + boxA).css('height'));
    };
    if (orderAll.length > 0) {
      checkHeight();
      $(window).load(function() {
        checkHeight();
      });
    }
    goNext = function() {
      return orderAll.find('.' + boxA).fadeOut(500, function() {
        return $(this).removeClass(boxA).next().addClass(boxA).fadeIn(500, function() {
          return checkHeight();
        });
      });
    };
    goBack = function() {
      return orderAll.find('.' + boxA).fadeOut(500, function() {
        return $(this).removeClass(boxA).prev().addClass(boxA).fadeIn(500, function() {
          return checkHeight();
        });
      });
    };
    orderNext.on('click', function() {
      goNext();
      return false;
    });
    orderBack.on('click', function() {
      goBack();
      return false;
    });
    $(".detail-box form").on("ajax:success", function(e, data, status, xhr) {
      return goNext();
    });
    sendPayWay = function() {
      return $.ajax({
        url: 'order/payway',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
          $('.active-way').removeClass('active-way');
          return modal.modalToggle($('.thanks-box'));
        }
      });
    };
    sendPayOnline = function() {
      return $.ajax({
        url: 'order/online',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
          return formSend(data);
        }
      });
    };
    formSend = function(data) {
      var d, form, v;
      form = '<form method="post" action="https://paysto.com/ru/pay/AuthorizeNet" style="display:none;" id="formForSend">';
      for (d in data) {
        v = data[d];
        form += "<input type='text' name='" + d + "' value='" + v + "' />";
      }
      form += "</form>";
      $(form).appendTo($('.cart-wrapper'));
      return $('#formForSend').get(0).submit();
    };
    $('.pay-buttons-panel a.pay-way-wrap.wallet').on("click", function() {
      $(this).addClass('active-way');
      sendPayWay();
      return false;
    });
    $('.pay-buttons-panel a.pay-way-wrap.online').on("click", function() {
      $(this).addClass('active-way');
      sendPayOnline();
      return false;
    });
    overlay = '.modal-overlay.order-special';
    $(overlay).on("click", function() {
      document.location.href = "/";
      return false;
    });
    return $('.modal-window.order-special .close-button').on("click", function() {
      document.location.href = "/";
      return false;
    });
  });

}).call(this);
