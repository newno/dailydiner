(function() {
  var hideNotice;

  hideNotice = function() {
    $('.alert').animate({
      opacity: 0.25,
      right: '-=400px'
    }, 500, function() {
      $(this).remove();
    });
  };

  $(document).on("page:change", function() {
    return setTimeout(hideNotice, 2000);
  });

}).call(this);
