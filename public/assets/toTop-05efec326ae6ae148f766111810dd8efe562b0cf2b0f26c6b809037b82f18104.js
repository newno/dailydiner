(function() {
  $(document).on("page:change", function() {
    return $('.toTop').on("click", function() {
      $('body, html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });

}).call(this);
