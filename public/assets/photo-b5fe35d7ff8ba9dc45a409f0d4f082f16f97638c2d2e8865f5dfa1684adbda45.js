(function() {
  var photoSlider;

  $.fn.extend({
    outerHTML: function(s) {
      if (s) {
        return this.before(s).remove();
      } else {
        return jQuery('<p>').append(this.eq(0).clone()).html();
      }
    },
    hideAnimate: function(aSpeed) {
      aSpeed = aSpeed || 300;
      this.animate({
        'opacity': '0'
      }, aSpeed, function() {
        return this.remove();
      });
      return this;
    },
    hideBox: function(aSpeed, c) {
      aSpeed = aSpeed || 300;
      this.animate({
        'opacity': '0'
      }, aSpeed, function() {
        this.css('display', 'none');
        return c();
      });
      return this;
    },
    animateToggle: function(aSpeed) {
      aSpeed = aSpeed || 300;
      this.animate({
        'opacity': 'toogle'
      }, {
        duration: aSpeed
      });
      return this;
    }
  });

  photoSlider = {
    w: function() {
      return $('.photo-head').data('boxW');
    },
    startSlide: function() {
      return setTimeout(this.slide, 5000);
    },
    slide: function() {
      var active, activeBox, head, last, left, next, puls;
      head = $('.photo-head');
      active = "showPhoto";
      activeBox = head.find('.photo-box.showPhoto');
      last = activeBox.eq(activeBox.length - 1);
      next = last.next();
      left = -photoSlider.w();
      if (head.find('.photo-box').length > 3) {
        if (head.find('.photo-box').length > 4) {
          puls = function() {
            activeBox.eq(0).removeClass(active);
            return next.addClass(active);
          };
        }
        head.animate({
          'left': left
        }, 500, function() {
          puls();
          $(activeBox.eq(0)).appendTo(head);
          return head.css('left', 0);
        });
      }
      return photoSlider.startSlide();
    },
    init: function() {
      $('body').addClass('slider-init');
      return this.startSlide();
    }
  };

  $(window).load(function() {
    if (!$('body').hasClass('slider-init')) {
      return photoSlider.init();
    }
  });

  $(document).on("page:change", function() {
    var boxW, w;
    $(document).on('ajax:success', '.delete-link', function(e, data, status, xhr) {
      return $(this).parents('.photo').hideAnimate();
    });
    if ($('.headerPhotos').length > 0) {
      if ($('.photo-head').find('.photo-box').length > 3) {
        w = $('.photo-head').width();
        boxW = w * 0.33 + 10;
        $('.photo-head').data('boxW', boxW).width(w + boxW);
      }
    }
  });

}).call(this);
