(function() {
  $(document).on("page:change", function() {
    $('.setSize').each(function() {
      return $(this).setSize();
    });
    $(window).load(function() {
      return $('body').addClass('ready');
    });
    $(document).on("click", "button.countDown", function() {
      return $(this).parents(".itemCounter").find("input[type=number]").val(function(index, x) {
        if (x > parseFloat($(this).attr('min'))) {
          return --x;
        } else {
          return x;
        }
      });
    });
    return $(document).on("click", "button.countUp", function() {
      return $(this).parents(".itemCounter").find("input[type=number]").val(function(index, x) {
        if (x < parseFloat($(this).attr('max'))) {
          return ++x;
        } else {
          return x;
        }
      });
    });
  });

}).call(this);
