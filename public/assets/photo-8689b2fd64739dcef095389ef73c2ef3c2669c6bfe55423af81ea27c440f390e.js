(function() {
  $.fn.extend({
    outerHTML: function(s) {
      if (s) {
        return this.before(s).remove();
      } else {
        return jQuery('<p>').append(this.eq(0).clone()).html();
      }
    },
    hideAnimate: function(aSpeed) {
      aSpeed = aSpeed || 300;
      this.animate({
        'opacity': '0'
      }, aSpeed, function() {
        return this.remove();
      });
      return this;
    },
    animateToggle: function(aSpeed) {
      aSpeed = aSpeed || 300;
      this.animate({
        'opacity': 'toogle'
      }, {
        duration: aSpeed
      });
      return this;
    }
  });

  $(document).on("page:change", function() {
    var addToHead, container, send, setting;
    setting = {
      revert: true,
      start: function() {
        return $(this).addClass('dragActive');
      },
      stop: function() {
        return $(this).removeClass('dragActive');
      },
      containment: ".main"
    };
    container = $('.photos-container');
    send = function(elem, position, target) {
      return $.ajax({
        url: '/photos/update',
        type: 'PATCH',
        data: {
          'id': parseInt(elem.get(0).id.match(/\d+$/)),
          'header_photo': {
            "position": position
          }
        },
        dataType: 'json',
        success: function(data) {
          target.addClass('success').find('i').fadeOut(1000);
          setTimeout((function() {
            return target.find('i').fadeIn(1000);
          }), 2000);
          return true;
        },
        error: function(data) {
          target.addClass('error').find('i').fadeOut(1000);
          setTimeout((function() {
            return target.find('i').fadeIn(1000);
          }), 2000);
          return false;
        }
      });
    };
    addToHead = function(elem, target) {
      var $this, item, newold, old;
      $this = elem;
      item = $this.find('.setSize');
      old = target.find('.setSize');
      newold = '<div class="photo draggable" id="';
      newold += target.get(0).id;
      newold += '"><a href="/photos/';
      newold += target.get(0).id.match(/\d+$/);
      newold += '" data-remote="true" data-method="delete" rel="nofollow" confirm="Вы уверены?" class="fa fa-times delete-link no-link"></a><div class="photo-overlay i-item"><i class="fa fa-hand-rock-o"></i></div>';
      newold += old.outerHTML();
      newold += "</div>";
      target.find('i').fadeIn(1000);
      target.removeClass('success error');
      send($this, target.index() + 1, target);
      if (old.length > 0) {
        $(newold).prependTo(container).draggable(setting);
        old.remove();
      }
      target.get(0).id = $this.get(0).id;
      target.append(item);
      $this.remove();
    };
    $(document).on('ajax:success', '.delete-link', function(e, data, status, xhr) {
      return $(this).parents('.photo').hideAnimate();
    });
    $('.draggable').draggable(setting);
    $('.header-photos .photo-box').droppable({
      drop: function(event, ui) {
        addToHead(ui.draggable, $(this));
      },
      hoverClass: 'hover',
      tolerance: 'fit'
    });
  });

}).call(this);
