(function() {
  this.orderPoller = {
    poll: function() {
      return setTimeout(this.request, 10000);
    },
    request: function() {
      return $.get($('#orderReady').data('url'), {
        after: $('#orderReady .order-ready').last().get(0).id
      });
    },
    addOrder: function(elem) {
      if ($(elem).length > 0) {
        $('#orders #order-body').append($(elem).addClass('new-order'));
        $('#notification-sound').get(0).play();
      }
      return this.poll();
    }
  };

  $(document).on("page:change", function() {
    if ($('#orders').length > 0) {
      orderPoller.poll();
    }
  });

}).call(this);
