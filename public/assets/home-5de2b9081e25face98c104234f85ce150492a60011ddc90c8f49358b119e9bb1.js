(function() {
  var onScroll;

  $(document).on("page:change", function() {
    var cart, cartOffset;
    if ($('.menuContainer').length > 0) {
      cart = $('.cart-link');
      cart.data('top', cart.css('top'));
      cartOffset = $('.menuContainer').offset().top;
      $('.menuContainer').data('offset', cartOffset);
      $(document).on('scroll', onScroll);
      return $('body').data('wh', $(window).height() / 2);
    }
  });

  onScroll = function(event) {
    var $wH, cart, cartOffset, cartTop, scrollPos;
    scrollPos = $(document).scrollTop();
    $wH = $('body').data('wh');
    cartOffset = $('.menuContainer').data('offset');
    cart = $('.cart-link');
    cartTop = cart.data('top');
    if (cartOffset - $wH >= scrollPos) {
      cart.addClass('stop');
    } else {
      cart.removeClass('stop');
    }
  };

}).call(this);
