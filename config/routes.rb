Rails.application.routes.draw do

  devise_for :users
  mount SeShop::Engine, at: "/menu"
  # root 'se_shop/category#index'
  root 'home#index'
  get '/logout' => 'user#logout', as: :logout
  get '/DonerStreet' => 'home#doner', as: 'donerstreet'
  get '/Cinnamon' => 'home#cinnamon', as: 'cinnamon'
  get '/contacts' => 'contacts#index', as: 'contacts'
  get '/pay_info' => 'contacts#pay_info', as: 'pay_info'

  %w( 404 422 500 ).each do |code|
    get code, :to => "errors#show", :code => code
  end
  resource :shop, path: "shop/:shop_id/", only:[] do
    resources :photos, except: [:update, :edit]
  end
  patch 'photos/update' => 'photos#update', as: 'photos_update'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
